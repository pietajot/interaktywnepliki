package pl.uep.kurs.cwiczenia;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InteraktywnePliki {

	public static void main(String[] args) {
		String sciezka = "C:\\Users\\piotrjanuszewski\\Documents\\osoby.csv";

		// wczytujemy list� os�b
		List<Osoba> osoby = czytajPlikCsv(sciezka);

		// rozpoczynamy skanowanie wej�cia
		Scanner skaner = new Scanner(System.in);
		String komenda = null;
		do {
			System.out.println("Co chcesz zrobi�?");
			komenda = skaner.nextLine();

			// Czytamy komendy i sprawdzamy czy jeste�my w stanie zinterpretowa� co u�ytkownik chce zrobi�
			if (komenda.equals("lista")) {
				if (osoby.size() == 0) {
					System.out.println("Lista os�b jest pusta");
				} else {
					System.out.println("--- Lista os�b ----");
					for (Osoba osoba : osoby) {
						System.out.println(osoby.indexOf(osoba) + ". " + osoba.getImie() + "\t" + osoba.getNazwisko());
					}					
				}
			} else if (komenda.equals("usu�") || komenda.equals("usun")) {
				System.out.println("Kt�r� osob� (podaj numer) chcesz usun��?");
				String sIndeks = skaner.nextLine();
				try {
					// usuwamy dan� osob� z listy, sprawdz
					int indeks = Integer.parseInt(sIndeks);
					Osoba o = osoby.get(indeks);
					System.out.println("Usuni�to osob� " + o.getImie() + " " + o.getNazwisko());
					osoby.remove(indeks);
					piszDoCsvOsoby(sciezka, osoby);
				} catch (NumberFormatException e) {
					System.out.println("Podany indeks nie jest poprawn� liczb� ca�kowit�");
				} catch (Exception e) {
					System.out.println("Podany indeks " + sIndeks + " nie istnieje, prosz� spr�bowa� jeszcze raz");
				}
			} else if (komenda.equals("dodaj")) {
				System.out.println("Podaj imi�:");
				String imie = skaner.nextLine();
				System.out.println("Podaj nazwisko:");
				String nazwisko = skaner.nextLine();
				Osoba osoba = new Osoba(imie, nazwisko);
				System.out.println("Dodano:");
				osoba.wyswietl();
				osoby.add(osoba);
				piszDoCsvOsoby(sciezka, osoby);
			} else if (komenda.equals("help")) {
				wyswietlDostepneKomendy();
			} else if (komenda.equals("quit") || komenda.equals("exit") || komenda.equals("koniec")) {
				// Wychodzimy z p�tli
				System.out.println("Do widzenia!");
				break;
			} else {
				// Nieznana komenda, niech uzytkownik zacznie od nowa
				System.out.println("Nieznana komenda. Dost�pne komendy:");
				System.out.println();
				wyswietlDostepneKomendy();
				komenda = null;
				continue;
			}
		} while (true);
	}

	private static void wyswietlDostepneKomendy() {
		System.out.println("lisa\tWy�wietla list� wszystkich os�b");
		System.out.println("dodaj\tDodaje osob� do listy");
		System.out.println("usu�\tUsuwa osob� z listy");
		System.out.println("help\tWy�wietla niniejsz� list� komend");
		System.out.println("quit | exit | koniec \tWyj�cie z programu");
	}

	private static ArrayList<String> czytajPlik(String sciezka) {
		ArrayList<String> przeczytaneLinie = new ArrayList<String>();
		try {
			FileReader plik = new FileReader(sciezka);
			BufferedReader czytacz = new BufferedReader(plik);

			String linia = null;
			while ((linia = czytacz.readLine()) != null) {
				przeczytaneLinie.add(linia);
			}
			czytacz.close();
		} catch (FileNotFoundException e) {
			System.out.println("Plik " + sciezka + " nie istnieje. Utworzono nowy.");
			piszDoPliku(sciezka, "");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return przeczytaneLinie;
	}

	private static void piszDoPliku(String sciezka, String zawartosc) {
		try {
			FileWriter plik = new FileWriter(sciezka);
			BufferedWriter pisacz = new BufferedWriter(plik);
			pisacz.write(zawartosc);
			pisacz.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static List<Osoba> czytajPlikCsv(String sciezka) {
		ArrayList<String> linie = czytajPlik(sciezka);
		List<Osoba> osoby = new ArrayList<Osoba>();
		for (String linia : linie) {
			String[] dane = linia.split(";");

			Osoba mojaNowaOsoba = new Osoba(dane[0], dane[1]);
			osoby.add(mojaNowaOsoba);
		}
		return osoby;
	}

	private static void piszDoCsv(String sciezka, ArrayList<String[]> dane) {
		String zawartoscPliku = "";
		for (String[] osoba : dane) {
			String linia = osoba[0] + ";" + osoba[1] + "\r\n";
			zawartoscPliku += linia;
		}
		piszDoPliku(sciezka, zawartoscPliku);
	}

	private static void piszDoCsvOsoby(String sciezka, List<Osoba> dane) {
		String zawartoscPliku = "";
		for (Osoba osoba : dane) {
			String linia = osoba.getImie() + ";" + osoba.getNazwisko() + "\r\n";
			zawartoscPliku += linia;
		}
		piszDoPliku(sciezka, zawartoscPliku);
	}

}
