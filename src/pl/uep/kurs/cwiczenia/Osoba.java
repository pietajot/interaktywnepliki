package pl.uep.kurs.cwiczenia;

public class Osoba {

	private String imie, nazwisko;
	
	public Osoba(String imie, String nazwisko) {
		this.imie = imie;
		this.nazwisko = nazwisko;
	}
	
	public void wyswietl()
	{
		System.out.println("---- Osoba ----");
		System.out.println("Imie " + this.imie + "\tNazwisko: " + this.nazwisko);
		System.out.println("---------------");
	}
	
	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
}
